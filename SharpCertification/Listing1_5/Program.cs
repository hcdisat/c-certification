﻿using System;
using System.Threading;

namespace Listing1_5
{
    public static class Program
    {

        [ThreadStatic]
        public static int _field;
        static void Main(string[] args)
        {
            new Thread(() => {
                
                for (int i = 0; i < 10; i++)
                {
                    Console.WriteLine("Thread A {0}", ++_field);                    
                }


            }).Start();

            new Thread(() => {
                
                for (int i = 0; i < 10; i++)
                {
                    Console.WriteLine("Thread B {0}", ++_field);
                }

            }).Start();

            Console.ReadLine();
        }
    }
}
