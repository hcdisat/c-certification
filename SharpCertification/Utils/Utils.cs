﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utils
{
    public static class Utils
    {
        public static string Wait(this object o)
        {
            return Console.ReadLine();
        }

        public static string Write(this object o)
        {
            string returnValue = o.ToString();
            Console.WriteLine(returnValue);
            return returnValue;
        }


    }
}
