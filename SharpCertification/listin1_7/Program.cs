﻿using System;
using System.Threading;

namespace listin1_7
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            ThreadPool.QueueUserWorkItem((s) => {
                Console.WriteLine("Working On a thread from threadpool");
            });

            Console.ReadLine();
        }
    }
}
