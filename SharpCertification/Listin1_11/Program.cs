﻿using System;
using System.Threading.Tasks;

namespace Listin1_11
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            Task<int> t = Task.Run(() => {
                return 66;
            });

            t.ContinueWith((i) => {
                Console.WriteLine("Canceled by User");
                Console.ReadLine();
            },TaskContinuationOptions.OnlyOnCanceled);

            t.ContinueWith((i) => {
                Console.WriteLine("Faulted");
            }, TaskContinuationOptions.OnlyOnFaulted);

            var completedTask = t.ContinueWith((i) => {
                Console.WriteLine("Hit ctrl + q to cancel");
                while (true)
                {
                    ConsoleKeyInfo key = Console.ReadKey();
                    if (key.Key == ConsoleKey.Q) {
                        Console.WriteLine("Implement Cancel Task");
                        System.Threading.Thread.CurrentThread.Abort();
                  
                    }
                }

                Console.WriteLine("Task Completed");                
            }, TaskContinuationOptions.OnlyOnRanToCompletion);

            completedTask.Wait();
            Console.ReadLine();
        }
    }
}
