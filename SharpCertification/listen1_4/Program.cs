﻿using System;
using System.Threading;

namespace listen1_4
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            bool stopped = false;

            Thread t = new Thread(new ThreadStart(() => {
                while ( !stopped)
                {
                    Console.WriteLine("Running.....");
                    Thread.Sleep(2000);
                }
            }));

            t.Start();
            
            Console.WriteLine("Press any key to exit");
            Console.ReadLine();

            stopped = true;
            t.Join();
        }
    }
}
