﻿using System;
using System.Threading.Tasks;

namespace Listin1_9
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            Task<int> t = Task.Run(() => {
                return 66;
            });

            Task<Person> p = Task.Run(() => {
                bool goOn = true;

                while (goOn) {

                    ConsoleKeyInfo key = Console.ReadKey();
                    if (key.Key == ConsoleKey.Q)
                        goOn = false;                       
                }

                return new Person { 
                    FullName  = "Hector Caraballo",
                    Id = 6
                };
            });

            Console.WriteLine(p.Result.ToString());
            Console.ReadKey();
        }
    }

    public class Person
    {
        public string FullName { get; set; }

        public int Id { get; set; }

        public override string ToString()
        {
            return String.Format("Name: {0}, Id: {1}", FullName, Id);
        }
    }
}
