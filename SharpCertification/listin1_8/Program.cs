﻿using System;
using System.Threading.Tasks;

namespace listin1_8
{
    public static class Program
    {
        public static void Main(string[] args)
        {            
            Task t = Task.Run(() => {
                for (int i = 0; i < 100; i++)
                {
                    Console.WriteLine("*");
                }
            });

            t.Wait();
            Console.ReadLine();
        }
    }
}
