﻿using System;
using System.Threading.Tasks;

namespace Listin1_10
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            Task<Person> p = Task.Run(() =>
            {
                return new Person
                {
                    FullName = "Hector Caraballo",
                    Id = 6
                };
            }).ContinueWith((s) => {
                s.Result.Id = 66;
                return s.Result;
            });

            Console.WriteLine(p.Result.ToString());
            Console.ReadKey();

        }
    }

    public class Person
    {
        public string FullName { get; set; }

        public int Id { get; set; }

        public override string ToString()
        {
            return String.Format("Name: {0}, Id: {1}", FullName, Id);
        }
    }
}
