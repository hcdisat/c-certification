﻿using System;
using System.Threading;
using Utils;

namespace listin1_6
{
    public static class Program
    {
        public static ThreadLocal<int> _field = new ThreadLocal<int>(() => {
            //ExecutionContext.SuppressFlow();
            return Thread.CurrentThread.ManagedThreadId;
        });
       
        public static void Main(string[] args)
        {
            new Thread(() => {                

                ("Field A" + _field).Write();

                for (int i = 0; i < _field.Value; i++)
                {
                    string output = String.Format("Thread A: {0}", i);
                    output.Write();
                }
            }).Start();

            new Thread(() => {
                
                ("Field B " + _field).Write();

                for (int i = 0; i < _field.Value; i++)
                {
                    string output = String.Format("Thread B: {0}", i);
                    output.Write();
                }
            }).Start();

            "".Wait();
        }
    }
}
